define(function(require, exports, module) {
    module.exports = function() {
        var app = angular.module('ProjectManage.controllers');
        app.controller('todoprojectCtrl', ['$scope', '$routeParams', '$location', '$rootScope',
            function($scope, $routeParams, $location, $rootScope) {
                var ratioTable = [{
                    itemName: '道床沉降',
                    ratio: 1
                }, {
                    itemName: '隧道平面位移',
                    ratio: 2
                }, {
                    itemName: '直径收敛',
                    ratio: 1.5
                }, {
                    itemName: '其他工作时间',
                    ratio: 1
                }];

                function applyRatio(items) {
                    items.forEach(function(item1, index) {
                        var filter = ratioTable.filter(function(item2) {
                            return item2.itemName === item1.itemName;
                        });
                        if (filter.length > 0) {
                            item1.ratio = filter[0].ratio;
                        }
                    });
                    return items;
                }

                function Item(n) {
                    this.ratio = 1;
                    this.itemName = "";
                    this.points = 0;
                    if (n != null) {
                        this.itemName = n;
                    }

                }

                function get_Items_From_LocalStorage(prjId) {
                    var items = JSON.parse(localStorage.getItem(prjId));
                    if (items) {
                        $scope.items = items;
                        SumPoints();
                    } else {
                        //初始化工作量
                        $scope.items = [];
                        var item = new Item("道床沉降");
                        $scope.items.push(item);
                        item = new Item("隧道平面位移");
                        $scope.items.push(item);
                        item = new Item("直径收敛");
                        $scope.items.push(item);
                        item = new Item("其他工作时间");
                        $scope.items.push(item);
                    }
                    items = applyRatio($scope.items);
                }


                //初始化所有变量$scope
                var index = $location.search().Index;
                var todoprojects = JSON.parse(localStorage.getItem("todoprojects"));
                if (todoprojects.length === 0) {
                    $location.path('/myprojects');
                }

                $scope.todoPrjId = todoprojects[index].PrjId;
                $scope.todoPrjName = todoprojects[index].PrjName;
                $scope.total = 0;
                $scope.hours = 0;
                get_Items_From_LocalStorage($scope.todoPrjId);



                function SumPoints() {
                    var total = 0;
                    var hours = 0;
                    for (var i = 0; i < $scope.items.length - 1; i++) {
                        var item = $scope.items[i];
                        total += item.ratio * item.points;
                    }
                    if (total == 0) {
                        hours = 0
                    } else if (total < 50) {
                        hours = 3;
                    } else if (total < 100) {
                        hours = 3 + 2 * (total - 50) / 50;
                    } else if (total < 250) {
                        hours = 5 + 5 * (total - 100) / 150;
                    } else {
                        hours = 10 + 6 * (total - 250) / 300;
                    }
                    hours = hours + $scope.items[$scope.items.length - 1].points;
                    $scope.total = Math.round(total * 100) / 100;
                    $scope.hours = Math.round(hours * 100) / 100;
                };

                //增加监视
                $scope.$watch("items", function() {
                    SumPoints();
                }, true)

                $scope.dateChange = function() {
                    var input = document.getElementById("dateInput");
                    $rootScope.date = input.value.toString();
                }



                $scope.nextStep = function() {
                    if ($scope.hours <= 0) {
                        alert("工时参数不正确!")
                        return;
                    } else {
                        todoprojects[index].Points = $scope.total;
                        todoprojects[index].Hours = $scope.hours;
                        todoprojects[index].Items = $scope.items;
                        todoprojects[index].Date = $rootScope.date.toString();
                        console.log($rootScope.date);

                        //储存
                        localStorage.setItem("todoprojects", JSON.stringify(todoprojects));
                        localStorage.setItem($scope.todoPrjId, JSON.stringify($scope.items));

                        if (Number(index) === (todoprojects.length - 1)) {
                            $location.path('/todostaff/').search({});

                        } else {
                            index = Number(index) + 1;
                            $location.path('/todoproject').search({
                                'Index': index
                            });
                        }
                    }
                }
            }
        ]);
    }
});
