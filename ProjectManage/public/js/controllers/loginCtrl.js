define(function(require, exports, module) {
	module.exports = function() {
		'use strict';
		var app = angular.module('ProjectManage.controllers');
		app.controller('loginCtrl', ['$rootScope', '$scope', '$location', '$http', 'initialModel',
			function($rootScope, $scope, $location, $http, initialModel) {

				$scope.rememberme = true;
				initialModel();
				$scope.login = function() {
					$http({
						method: "POST",
						url: "/users/",
						data: {
							name: $rootScope.user.name,
							psw: $rootScope.user.psw
						}
					}).success(function(data) {

						if (JSON.parse(data) == true) {
							$rootScope.login = true;
							if ($scope.rememberme == true) {
								var userName = localStorage.setItem("username", $rootScope.user.name);
							}
							$rootScope.searchTxt = $rootScope.user.name;
							$location.path('/home');
						} else {
							$scope.logInFail = true;
							$rootScope.user.psw = "";
						}
					});
				}
				$scope.logInFail = false;
			}
		]);
	}
});