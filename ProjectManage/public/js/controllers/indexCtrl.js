define(function(require, exports, module) {
	module.exports = function() { /* Controllers */
		'use strict';
		var app = angular.module('ProjectManage.controllers');
		app.controller('indexCtrl', ['$rootScope', '$scope', '$location', '$http',
			function($rootScope, $scope, $location, $http) {
				$scope.logout = function() {
					var userName = localStorage.setItem("username", '');
					$rootScope.user = {
						name: '',
						psw: ''
					};
					$location.path('/login');
					$rootScope.login = false;
				}

				$scope.addproject = function() {
					var prj = {};
					localStorage.setItem('prj', JSON.stringify(prj));
					$location.path('/addproject');
				}

				$scope.addstaff = function() {
					var staff = {};
					localStorage.setItem('staff', JSON.stringify(staff));
					$location.path('/addstaff');
				}
			}
		]);
	};
});