define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.controllers');
		app.controller('successCtrl', ['$scope', '$location', '$timeout', '$rootScope', function($scope, $location, $timeout, $rootScope) {
			$scope.timeOut = 5;
			var timeoutId;
			$rootScope.todostaffs = [];
			$rootScope.todoprojects = [];
			localStorage.setItem("todostaffs", "");
			localStorage.setItem("todoprojects", "");

			function timer() {
				timeoutId = $timeout(function() {
					$scope.timeOut--;
					if ($scope.timeOut === 0) {
						//$timeout.cancel(timeoutId);
						$location.path('/myprojects/');
					} else {
						timer();
					}
				}, 1000);
			}
			timer();

		}]);
	}
});