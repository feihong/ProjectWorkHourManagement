define(function(require, exports, module) {
	module.exports = function() {
		'use strict';

		/* Filters */

		angular.module('ProjectManage.filters', []).
		filter('interpolate', ['version',
			function(version) {
				return function(text) {
					return String(text).replace(/\%VERSION\%/mg, version);
				}
			}
		]);
	}
});