define(function(require, exports, module) {
	module.exports = function() {
		var app = angular.module('ProjectManage.directives');
		app.directive('autoLogIn', ['$location', '$rootScope', 'initialModel',
			function($location, $rootScope, initialModel) {
				return {
					restrict: 'A',
					link: function(scope, iElement, iAttrs) {
						if ($rootScope.login == undefined || $rootScope.login == false) {
							//第一次登陆,初始化所有的用户数据.
							initialModel();
							var userName = localStorage.getItem("username");
							if (userName != null && userName != "") {
								scope.onceLogIn = true;
								$rootScope.login = true;
								$rootScope.user.name = userName;
								$rootScope.searchTxt = userName;
							} else {
								$location.path('/login');
							}
						}
					}
				};
			}
		]);
	}
});