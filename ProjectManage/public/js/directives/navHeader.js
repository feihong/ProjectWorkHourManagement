define(function(require, exports, module) {
	module.exports = function() {
		'use strict';
		var app = angular.module('ProjectManage.directives');
		app.directive('navHeader', function() {
			return {
				restrict: 'AE',
				templateUrl: 'components/navHeader.html'
			};
		});
	};
});