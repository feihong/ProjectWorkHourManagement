define(function(require, exports, module) {
	module.exports = function() {
		'use strict';

		require('./filters.js')();
		require('./directives/directives.js')();
		require('./controllers/controllers.js')();
		require('./services/index.js')();
		require('./routes.js')();
		angular.module('ProjectManage', [
			'ui.bootstrap',
			'ngRoute',
			'ngAnimate',
			'ProjectManage.routes',
			'ProjectManage.filters',
			'ProjectManage.services',
			'ProjectManage.directives',
			'ProjectManage.controllers'
		]);

		//手动启动app
		var htmlElement = document.getElementsByTagName("html")[0];
		//htmlElement.setAttribute("data-ng-app", "ProjectManage");
		var $html = angular.element(htmlElement);
		$html.attr("data-ng-app", "ProjectManage");
		angular.element().ready(function() {
			angular.bootstrap($html, ['ProjectManage']);
		});
	}
});