define(function(require, exports, module) {
	module.exports = function() {
		'use strict';

		/* Services */


		// Demonstrate how to register services
		// In this case it is a simple value service.
		var app = angular.module('ProjectManage.services');
		app.factory('initialModel', ['$rootScope', '$filter',
			function($rootScope, $filter) {
				return function() {
					//定义用户名称和密码
					$rootScope.user = {
						name: '',
						psw: ''
					};
					//用户是否已经登陆
					$rootScope.login = false;
					//收藏的工程
					$rootScope.myprojects = [];
					//收藏的员工
					$rootScope.mystaffs = [];

					//所有的工程
					$rootScope.projects = [];
					//所有的员工
					$rootScope.staffs = [];


					//待操作的所有工程列表
					$rootScope.todoprojects = [];

					//待操作的所有工程列表
					$rootScope.todostaffs = [];



					//当前的评估时间
					$rootScope.date = $filter('date')(new Date(), 'yyyy-MM-dd').toString();
					$rootScope.SortStaffByChecked = function(staff1, staff2) {
						if (staff1.checked === false && staff2.checked === true) {
							return 1;
						} else {
							return -1;
						}
					}

				}

			}
		])
	}
});