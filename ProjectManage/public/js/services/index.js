define(function(require, exports, module) {
	module.exports = function() {
		'use strict';

		/* Services */


		// Demonstrate how to register services
		// In this case it is a simple value service.
		angular.module('ProjectManage.services', []);
		require('./initialModel.js')();
		require('./myProjectsFunc.js')();
		require('./allProjectsFunc.js')();
		require('./myStaffsFunc.js')();
		require('./allStaffsFunc.js')();

	}
});