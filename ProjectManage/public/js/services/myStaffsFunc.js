define(function(require, exports, module) {
	module.exports = function() {
		'use strict';
		var app = angular.module('ProjectManage.services');
		app.factory('myStaffsFunc', ['$rootScope', '$http', function($rootScope, $http) {
			return {
				getMyStaffs: function(callback) {
					if ($rootScope.mystaffs.length == 0) {
						$http({
							url: '/favoriteStaffs/' + $rootScope.user.name,
							method: 'get'
						}).success(function(data) {
							$rootScope.mystaffs = data;
							angular.forEach($rootScope.mystaffs, function(staff) {
								//默认不勾选该员工
								staff.checked = false;
							});
							if (callback != undefined) {
								callback();
							}
						});
					} else {
						if (callback != undefined) callback();
					}
				},
				addMyStaff: function(staff, callback) {
					staff.checked = true;
					$rootScope.mystaffs.push(staff);
					if (callback != undefined) callback();
				},
				removeMyStaff: function(staff, callback) {
					staff.checked = false;
					for (var i = 0; i < $rootScope.mystaffs.length; i++) {
						if ($rootScope.mystaffs[i].Name === staff.Name) {
							$rootScope.mystaffs.splice(i, 1);
							break;
						}
					}
					if (callback != undefined) callback();
				},
				addFavorStaffs: function(staffList) {
					if (staffList.length > 0) {
						$http({
							url: '/favoriteStaffs',
							method: 'put',
							data: {
								'name': $rootScope.user.name,
								'staffList': staffList
							}
						}).success(function(data) {});
					}

				},
				removeFavorStaffs: function(staffList) {
					if (staffList.length > 0) {
						$http({
							url: '/favoriteStaffs',
							method: 'post',
							data: {
								'name': $rootScope.user.name,
								'staffList': staffList
							}
						}).success(function(data) {});
					}
				}

			};
		}])
	}
});