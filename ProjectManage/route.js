module.exports = function(app) {
	app.use(app.router);
	app.use(function(req, res) {
		// Use res.sendfile, as it streams instead of reading the file into memory.
		res.sendfile(__dirname + '/public/index.html');
	});

	//api definition	
	var user = require('./routes/user');
	app.post('/users', user.login);

	var favoritePrj = require('./routes/favoriteProjects');
	app.get('/favoriteProjects/:username/:userid?', favoritePrj.getFavoritePrjs); //get in array
	app.put('/favoriteProjects', favoritePrj.putFavoritePrjs); //insert in array
	app.post('/favoriteProjects', favoritePrj.postFavoritePrjs); //delete in array form


	var favoriteStaffs = require('./routes/favoriteStaffs');
	app.get('/favoriteStaffs/:username/:userid?', favoriteStaffs.getFavoriteStaffs); //get in array
	app.put('/favoriteStaffs', favoriteStaffs.putFavoriteStaffs); //insert in array
	app.post('/favoriteStaffs', favoriteStaffs.postFavoriteStaffs); //delete in array form


	var projects = require('./routes/projects');
	app.get('/projects', projects.getProjects);
	app.put('/projects', projects.putProject);

	var staffs = require('./routes/staffs');
	app.get('/staffs', staffs.getStaffs);
	app.put('/staffs', staffs.putStaff);

	var workhours = require('./routes/workhours');
	app.post('/workhours', workhours.postWorkHours);


}