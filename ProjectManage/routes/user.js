exports.login = function(req, res) {
	var request = require('request');
	var api = global.apiUrl + '/users';
	var name = req.body.name;
	var psw = req.body.psw;
	request({
		method: 'POST',
		json: JSON.stringify({
			'name': name,
			'psw': psw
		}),
		url: api
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(body);
		} else {
			res.send(false);
		}
	});
}