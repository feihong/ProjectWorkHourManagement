exports.getFavoritePrjs = function(req, res) {
	var request = require('request');

	var name = req.params.username;
	request({
		url: global.apiUrl + '/favorproject/',
		qs: {
			'name': name
		}
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(JSON.parse(body));
		} else {
			res.send('fail to load!');
		}
	});
};
exports.postFavoritePrjs = function(req, res) {
	var request = require('request');
	var name = req.body.name;
	var prjList = req.body.prjList;
	request({
		method: 'POST',
		url: global.apiUrl + '/favorproject/',
		json: JSON.stringify({
			'name': name,
			'prjList': prjList
		})
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(body);
		} else {
			res.send(false);
		}
	});
};
exports.putFavoritePrjs = function(req, res) {
	var request = require('request');
	var name = req.body.name;
	var prjList = req.body.prjList;
	request({
		method: 'PUT',
		url: global.apiUrl + '/favorproject/',
		json: JSON.stringify({
			'name': name,
			'prjList': prjList
		})
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(true);
		} else {
			res.send(false);
		}
	});
};


// var querystring = require('querystring');
// var api = global.apiUrl + '/favorproject/?' + querystring.stringify({
// 	'name': '刘飞'
// });