exports.getFavoriteStaffs = function(req, res) {
	var request = require('request');

	var name = req.params.username;
	request({
		url: global.apiUrl + '/favorstaff/',
		qs: {
			'name': name
		}
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(JSON.parse(body));
		} else {
			res.send('fail to load!');
		}
	});
};
exports.postFavoriteStaffs = function(req, res) {
	var request = require('request');
	var name = req.body.name;
	var staffList = req.body.staffList;
	request({
		method: 'POST',
		url: global.apiUrl + '/favorstaff/',
		json: JSON.stringify({
			'name': name,
			'staffList': staffList
		})
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(body);
		} else {
			res.send(false);
		}
	});
};
exports.putFavoriteStaffs = function(req, res) {
	var request = require('request');
	var name = req.body.name;
	var staffList = req.body.staffList;
	request({
		method: 'PUT',
		url: global.apiUrl + '/favorstaff/',
		json: JSON.stringify({
			'name': name,
			'staffList': staffList
		})
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(true);
		} else {
			res.send(false);
		}
	});
};


