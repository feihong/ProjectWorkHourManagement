*
 * GET home page.
 */

exports.index = function(req, res) {
	res.render('index', {
		title: 'Express'
	});
};
// request({
// 	method: 'POST',
// 	json: JSON.stringify({
// 		value: "heydude"
// 	}),
// 	url: 'http://localhost:52482/api/values'
// }, function(error, response, body) {
// 	if (!error && response.statusCode == 200) {
// 		res.send(body);
// 	} else {
// 		res.send('fail to load!');
// 	}
// })

var request = require('request');
request({
	method: 'POST',
	json: JSON.stringify({
		value: "heydude"
	}),
	url: 'http://localhost:52482/api/values'
}, function(error, response, body) {
	if (!error && response.statusCode == 200) {
		res.send(body);
	} else {
		res.send('fail to load!');
	}
})