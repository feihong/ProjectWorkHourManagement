﻿using Newtonsoft.Json.Linq;
using ProjectStatic.Models;
using ProjectStatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectStatic.Services
{
    public class StaffArrayInsertService
    {
        public StaffArrayInsertService(string date, string teamleader, JArray array)
        {
            List<string> list = new List<string>();
            foreach (var item in array)
            {
                string aa = item["Name"].ToString();
                Employee em = new Employee()
                {
                    Date = date,
                    Name = item["Name"].ToString(),
                    TeamLeader = teamleader,
                    WorkHour = item["Hours"].ToObject<double>(),
                    WorkType = item["WorkType"].ToString()
                };
                if (em.WorkHour > 0)
                {
                    string sql = string.Format("delete from employees where 班组长='{0}' and 姓名='{1}' and 日期='{2}'",
                        teamleader, em.Name, date);
                    list.Add(sql);
                    list.Add(em.GenerateInsertSqlStr());
                }
            }
            DbHelperSQL.ExecuteSqlTran(list);
        }
    }
}