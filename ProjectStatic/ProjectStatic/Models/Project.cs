﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace ProjectStatic.Models
{
    [Table("projects")]
    public class Project : BaseModel
    {
        public Project() { }
        [Column("项目编号")]
        [Display(Name = "项目编号")]
        public string PrjId { get; set; }

        [Column("项目名称")]
        public string PrjName { get; set; }

        [Column("项目经理")]
        public string PrjManager { get; set; }

        [Column("班组长")]
        public string PrjTeamLeader { get; set; }

        [Column("项目类型")]
        public string PrjType { get; set; }

        [Column("难度系数")]
        public double PrjRatio { get; set; }

        [Column("备注3")]
        public string Mark1 { get; set; }

        [Column("备注4")]
        public string Mark2 { get; set; }
    }
}