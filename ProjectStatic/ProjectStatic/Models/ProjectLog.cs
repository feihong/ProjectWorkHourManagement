﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace ProjectStatic.Models
{


    [Table("projectlog")]
    public class ProjectLog : BaseModel
    {
        public ProjectLog()
        {
            this.No = 0;
            this.Note1 = "";
            this.Note2 = "";
            this.Note3 = "";
        }


        [Column("项目编号")]
        public string PrjId { get; set; }
        [Column("班组长")]
        public string TeamLeader { get; set; }
        [Column("日期")]
        public string Date { get; set; }
        [Column("次数")]
        public double No { get; set; }
        [Column("点数")]
        public double Points { get; set; }
        [Column("备注1")]
        public string Note1 { get; set; }
        [Column("备注2")]
        public string Note2 { get; set; }
        [Column("备注3")]
        public string Note3 { get; set; }


    }
}