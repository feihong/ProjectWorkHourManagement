﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectStatic.Models
{
    [Table("FavorProject")]
    public class FavorProject : BaseModel
    {
        [Column("Name")]
        public string Name { get; set; }

        [Column("FavorPrjId")]
        public string FavorPrjId { get; set; }

        [Column("Note")]
        public string Note { get; set; }
    }
}