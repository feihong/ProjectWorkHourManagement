﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace ProjectStatic.Models
{
    [Table("employees")]
    public class Employee : BaseModel
    {

        public Employee()
        {
            this.Performance = 1;
            this.WorkDuration = 8;
        }

        [Column("姓名")]
        public string Name { get; set; }
        [Column("班组长")]
        public string TeamLeader { get; set; }
        [Column("日期")]
        public string Date { get; set; }
        [Column("工时数量")]
        public double WorkHour { get; set; }
        [Column("出勤时长")]
        public double WorkDuration { get; set; }
        [Column("个人表现")]
        public double Performance { get; set; }
        [Column("班次")]
        public string WorkType { get; set; }

    }
}