﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectStatic.Models;
using ProjectStatic.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace ProjectStatic.Controllers
{
    public class WorkHoursController : ApiController
    {
        // GET api/workhours
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/workhours/5
        public string Get(int id)
        {
            return "value";
        }

        public void Post([FromBody]string value)
        {
            JObject jsonData = JObject.Parse(value);
            string name = jsonData["name"].ToString();
            string date = jsonData["date"].ToString();
            JArray todoprojects = jsonData["todoprojects"].ToObject<JArray>();
            JArray todostaffs = jsonData["todostaffs"].ToObject<JArray>();
            foreach (var item in todoprojects)
            {
                //解析和储存进数据库 
                ProjectInsertService record = new ProjectInsertService(name, item);
            }
            StaffArrayInsertService sevice = new StaffArrayInsertService(date, name, todostaffs);

        }

        // PUT api/workhours/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/workhours/5
        public void Delete(int id)
        {
        }
    }
}
