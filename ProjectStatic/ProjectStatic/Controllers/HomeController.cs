﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectStatic.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
            
        }

        public JsonResult String()
        {
            return Json("this is json result",JsonRequestBehavior.AllowGet);  
        }
    }
}
